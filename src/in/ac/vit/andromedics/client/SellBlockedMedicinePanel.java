/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SellBlockedMedicinePanel.java
 *
 * Created on Apr 10, 2013, 12:41:30 PM
 */

package in.ac.vit.andromedics.client;

/**
 *
 * @author Satya
 */
public class SellBlockedMedicinePanel extends javax.swing.JFrame {

    /** Creates new form SellBlockedMedicinePanel */
    public SellBlockedMedicinePanel() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
    * @param args the command line arguments
    */
  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables


    public static SellBlockedMedicinePanel sellBlockedMedicinePanel=null;
    public  static SellBlockedMedicinePanel getObjectInstance() {
		if (sellBlockedMedicinePanel == null) {
			try {
				sellBlockedMedicinePanel = new SellBlockedMedicinePanel();
                                System.out.println("New Home object created.");
			} catch ( Exception e) {
				e.printStackTrace();
			}
			return sellBlockedMedicinePanel;
		} else {
			return sellBlockedMedicinePanel;
		}

	}
}
