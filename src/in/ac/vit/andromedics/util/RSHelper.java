/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.vit.andromedics.util;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import javax.ws.rs.core.MediaType;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Satya
 */
public class RSHelper {

    private final static String BASE_URL = "http://andromedics-mtechst.rhcloud.com/andromedics-rest/api/";

    public static JSONObject postRequest(String url, JSONObject object) {
        try {

            Client client = Client.create();

            WebResource webResource = client.resource(BASE_URL + url);
            client.addFilter(new HTTPBasicAuthFilter(UserConfiguration.USERNAME, UserConfiguration.PASSWORD));

            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, object);

            if (response.getStatus() != 200) {
                throw new RuntimeException(response.getStatus() + "");
            }

            String output = response.getEntity(String.class);
            JSONObject json = new JSONObject(output);
            return json;

        } catch (Exception e) {
            if (e.getMessage().equals("401")) {
                throw new RuntimeException("401");
            }
            e.printStackTrace();
            System.out.println(e.getClass());
        }
        return null;
    }

    public static JSONObject getRequest(String url, String object) {
        try {

            Client client = Client.create();

            WebResource webResource = client.resource(BASE_URL + url + "/" + object);
            client.addFilter(new HTTPBasicAuthFilter(UserConfiguration.USERNAME, UserConfiguration.PASSWORD));

            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON)
                    .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException(response.getStatus() + "");
            }

            String output = response.getEntity(String.class);
            JSONObject json = new JSONObject(output);
            return json;

        } catch (Exception e) {
            if (e.getMessage().equals("401")) {
                throw new RuntimeException("401");
            }
            e.printStackTrace();
            System.out.println(e.getClass());
        }
        return null;
    }
}
