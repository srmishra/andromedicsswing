/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package in.ac.vit.andromedics.util;

import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Satya
 */
public class UserConfiguration {
    public static String USERNAME="";
    public static String PASSWORD="";
    public static String STORE_ID="";
    public static String BRANCH_ID="";
    public static String NAME="";
    public static void flush(){
        USERNAME="";
        PASSWORD="";
        STORE_ID="";
        BRANCH_ID="";
        NAME="";
    }
    public static void initialize(JSONObject data){
        try{
        UserConfiguration.STORE_ID = data.getString("storeId");
                UserConfiguration.BRANCH_ID = data.getString("branchId");
                UserConfiguration.NAME = data.getString("fullname");
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

}
